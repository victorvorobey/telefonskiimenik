package si.src.testtask.telimenik.view;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.apache.commons.collections4.MapUtils;
import si.src.testtask.common.dto.ContactDTO;
import si.src.testtask.telimenik.SecurityContext;
import si.src.testtask.telimenik.service.ContactService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
@ManagedBean(name = "newContactView")
@ViewScoped
@Log
public class NewContactView implements Serializable {

    private static final long serialVersionUID = -9142115221458986555L;

    @Inject
    private SecurityContext securityContext;

    @ManagedProperty("#{userContactsView}")
    private UserContactsBean userContactsBean;

    @ManagedProperty("#{contactService}")
    private ContactService userContactService;

    private ContactDTO newContact = new ContactDTO();

    public NewContactView() {
        System.out.println();
    }

    public void save() {
        userContactService.saveContact(newContact);

        // Update the Contacts state to reload it after saving new one
        userContactsBean.filter();
        FacesMessage msg = new FacesMessage("Contact created", String.format("New contact: \"%s %s\" has been created.", newContact.getFirstName(), newContact.getLastName()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
        newContact = new ContactDTO(); // Clear the form after creation
        log.info("New contact saved");
    }
}
