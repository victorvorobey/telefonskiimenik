package si.src.testtask.telimenik.view;

import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.RowEditEvent;
import si.src.testtask.common.dto.ContactDTO;
import si.src.testtask.telimenik.SecurityContext;
import si.src.testtask.telimenik.service.ContactService;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@ManagedBean(name = "userContactsView")
@ViewScoped
@Getter
@Setter
public class UserContactsBean implements Serializable {

    private static final long serialVersionUID = -3845777996394742340L;

    @ManagedProperty("#{contactService}")
    private ContactService userContactService;

    @Inject
    private SecurityContext securityContext;

    private List<ContactDTO> contacts;
    private ContactDTO selectedContact;

    private String filterValue;

    @PostConstruct
    public void init() {
        contacts = userContactService.getContacts();
    }

    public void logout() throws ServletException {
        securityContext.logout();
    }

    public void onRowEdit(RowEditEvent event) {
        ContactDTO toUpdate = (ContactDTO) event.getObject();
        userContactService.updateContact(toUpdate);

        FacesMessage msg = new FacesMessage("Contact Edited", String.format("Contact: \"%s %s\" has been updated.", toUpdate.getFirstName(), toUpdate.getLastName()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowEditCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void filter() {
        if (filterValue == null || filterValue.trim().isEmpty()) {
            init();
        } else {
            contacts = userContactService.filter(filterValue.trim());
        }
    }

    public void deleteContact(ContactDTO contactDTO) {
        if (contactDTO != null) {
            userContactService.deleteContact(contactDTO);
            filter();
            FacesMessage msg = new FacesMessage("Contact removed", String.format("Contact: \"%s %s\" has been removed.", contactDTO.getFirstName(), contactDTO.getLastName()));
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public String getLoggedUserName() {
        return securityContext.getLoggedUserName();
    }
}
