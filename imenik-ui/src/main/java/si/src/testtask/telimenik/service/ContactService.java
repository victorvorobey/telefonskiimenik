package si.src.testtask.telimenik.service;

import lombok.extern.java.Log;
import org.apache.commons.collections4.MapUtils;
import si.src.testtask.common.dto.ContactDTO;
import si.src.testtask.telimenik.SecurityContext;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

@ManagedBean(name = "contactService")
@ApplicationScoped
@Log
public class ContactService {

    private static final GenericType<List<ContactDTO>> CONTACT_LIST_GENERIC_TYPE =
            new GenericType<List<ContactDTO>>() {};

    private static final GenericType<Map<String, String>> VALIDATION_ERROR_TYPE =
            new GenericType<Map<String, String>>() {};

    private String baseUri = "http://localhost:8080/imenik-api/me/contacts";
    private Client jaxRsClient;

    @Inject
    private SecurityContext securityContext;

    public ContactService() {
        jaxRsClient = ClientBuilder.newClient();
    }

    public List<ContactDTO> getContacts() {
        return jaxRsClient.target(baseUri)
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", String.format("Bearer %s", securityContext.getTokenString()))
                .get(CONTACT_LIST_GENERIC_TYPE);
    }

    public void saveContact(ContactDTO contactDTO) {
        jaxRsClient.target(baseUri)
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", String.format("Bearer %s", securityContext.getTokenString()))
                .post(Entity.entity(contactDTO, MediaType.APPLICATION_JSON));
    }

    public void updateContact(ContactDTO contactDTO) {
        jaxRsClient.target(baseUri)
                .path(contactDTO.getId().toString())
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", String.format("Bearer %s", securityContext.getTokenString()))
                .put(Entity.entity(contactDTO, MediaType.APPLICATION_JSON));
    }

    public void deleteContact(ContactDTO contactDTO) {
        jaxRsClient.target(baseUri)
                .path(contactDTO.getId().toString())
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", String.format("Bearer %s", securityContext.getTokenString()))
                .delete();
    }

    public List<ContactDTO> filter(String filterValue) {
        return jaxRsClient.target(baseUri)
                .path("filter")
                .queryParam("filter", filterValue)
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", String.format("Bearer %s", securityContext.getTokenString()))
                .get(CONTACT_LIST_GENERIC_TYPE);
    }
}
