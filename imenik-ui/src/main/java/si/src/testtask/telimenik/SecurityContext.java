package si.src.testtask.telimenik;

import lombok.extern.java.Log;
import org.keycloak.KeycloakSecurityContext;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@RequestScoped
@Log
public class SecurityContext {

    @Inject
    private HttpServletRequest request;

    public String getTokenString() {
        final KeycloakSecurityContext ksc = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        final String token = ksc == null ? "" : ksc.getTokenString();

        log.info("Requested token: \"" + token + "\"");
        return token;
    }

    public String getLoggedUserName() {
        final KeycloakSecurityContext ksc = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return ksc == null ? "" : ksc.getToken().getName();

    }

    public void logout() throws ServletException {
        request.logout();
    }
}
