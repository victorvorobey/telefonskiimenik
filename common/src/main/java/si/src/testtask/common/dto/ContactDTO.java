package si.src.testtask.common.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@Getter
@Setter
public class ContactDTO implements BaseDTO {

    private Long id;
    @NotEmpty
    @Size(min = 2, max = 30)
    private String firstName;
    @NotEmpty
    @Size(min = 2, max = 30)
    private String lastName;
    @NotEmpty

    private String address;
    private String phoneNumber;
    private String mobilePhoneNumber;

    @NotEmpty
    @Email(message = "{contact.email}")
    private String email;
    @Size(max = 1000)
    private String note;
}
