package si.src.testtask.imenikapi.entity;

import lombok.Getter;
import lombok.Setter;
import si.src.testtask.imenikapi.utils.CryptoConverter;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@Entity
@Table(indexes = {
        @Index(name = "USER_CONTACT_ACCOUNT_ID_IDX", columnList = "ACCOUNT_ID")
})
@SqlResultSetMappings(
        @SqlResultSetMapping(name = "ContactEntity",
                entities = @EntityResult(entityClass = Contact.class,
                        fields = {
                                @FieldResult(name = "id", column = "id"),
                                @FieldResult(name = "firstName", column = "firstName"),
                                @FieldResult(name = "lastName", column = "lastName"),
                                @FieldResult(name = "address", column = "address"),
                                @FieldResult(name = "phoneNumber", column = "phoneNumber"),
                                @FieldResult(name = "mobilePhoneNumber", column = "mobilePhoneNumber"),
                                @FieldResult(name = "note", column = "note"),
                                @FieldResult(name = "email", column = "email"),
                                @FieldResult(name = "accountId", column = "accountId"),
                        }))
)
@NamedNativeQueries(
        @NamedNativeQuery(name = Contact.FILTER_BY_FIRST_LAST_OR_PHONE,
                resultSetMapping = "ContactEntity",
                query = "SELECT c.ID as id, " +
                        "CONVERT(AES_DECRYPT(FROM_BASE64(c.FIRST_NAME), ?1), CHAR) as firstName, " +
                        "CONVERT(AES_DECRYPT(FROM_BASE64(c.LAST_NAME), ?1), CHAR) as lastName, " +
                        "CONVERT(AES_DECRYPT(FROM_BASE64(c.ADDRESS), ?1), CHAR) as address, " +
                        "CONVERT(AES_DECRYPT(FROM_BASE64(c.PHONE_NUMBER), ?1), CHAR) as phoneNumber, " +
                        "CONVERT(AES_DECRYPT(FROM_BASE64(c.MOBILE_PHONE_NUMBER), ?1), CHAR) as mobilePhoneNumber, " +
                        "CONVERT(AES_DECRYPT(FROM_BASE64(c.NOTE), ?1), CHAR) as note, " +
                        "CONVERT(AES_DECRYPT(FROM_BASE64(c.EMAIL), ?1), CHAR) as email, " +
                        "c.ACCOUNT_ID as accountId " +
                        "FROM CONTACT as c where c.ACCOUNT_ID = ?2 AND " +
                        "(UPPER(CONVERT(AES_DECRYPT(FROM_BASE64(c.FIRST_NAME), ?1), CHAR)) like ?3 OR " +
                        "UPPER(CONVERT(AES_DECRYPT(FROM_BASE64(c.LAST_NAME), ?1), CHAR)) like ?3 OR " +
                        "UPPER(CONVERT(AES_DECRYPT(FROM_BASE64(c.PHONE_NUMBER), ?1), CHAR)) like ?3 OR " +
                        "UPPER(CONVERT(AES_DECRYPT(FROM_BASE64(c.MOBILE_PHONE_NUMBER), ?1), CHAR)) like ?3)"
        )
)
@NamedQueries({
        @NamedQuery(name = Contact.FIND_BY_ACCOUNT, query = "SELECT c FROM Contact c where c.accountId = :accountId"),
})
public class Contact implements IPersistent<Long> {

    private static final long serialVersionUID = -7316207209892517231L;

    public static final String FILTER_BY_FIRST_LAST_OR_PHONE = "Contact.filterByFilrstLastOrPhone";
    public static final String FIND_BY_ACCOUNT = "Contact.findByAccount";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @Convert(converter = CryptoConverter.class)
    @Column(name = "FIRST_NAME", length = 30)
    private String firstName;

    @Convert(converter = CryptoConverter.class)
    @Column(name = "LAST_NAME", length = 30)
    private String lastName;

    @Convert(converter = CryptoConverter.class)
    @Column(name = "ADDRESS")
    private String address;

    @Convert(converter = CryptoConverter.class)
    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Convert(converter = CryptoConverter.class)
    @Column(name = "MOBILE_PHONE_NUMBER")
    private String mobilePhoneNumber;

    @Convert(converter = CryptoConverter.class)
    @Column(name = "NOTE", length = 1000)
    private String note;

    @Convert(converter = CryptoConverter.class)
    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ACCOUNT_ID", nullable = false)
    private String accountId;

    @Setter
    @Column(name = "UPDATED")
    private Timestamp updated = new Timestamp(System.currentTimeMillis());

    @PreUpdate
    public void preUpdate() {
        this.updated = new Timestamp(System.currentTimeMillis());
    }
}