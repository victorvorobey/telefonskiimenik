package si.src.testtask.imenikapi.rest;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import lombok.extern.java.Log;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import si.src.testtask.common.dto.ContactDTO;
import si.src.testtask.imenikapi.entity.Contact;
import si.src.testtask.imenikapi.service.ContactService;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.List;

import static si.src.testtask.imenikapi.utils.ContactCSVBodyWriter.MEDIA_TYPE_CSV;

/**
 * The class represents the Rest resource to access the Contacts.
 * It allows to manipulate with the Contacts which belong to the currently logged in user only.
 * Thus, only authorized users can have access for this resource.
 */
@OpenAPIDefinition(
        info = @Info(
                title = "API to manipulate with users' contacts",
                version = "1.0"
        ),
        security = {
                @SecurityRequirement(name = "Requires Authorization header (Bearer token)")
        },
        servers = @Server(url = "/me/contacts")
)
@Stateless
@Path("/me/contacts/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({"role-telephone-directory-api"})
@Log
public class UserContactBookResource {

    private static final Type CONTACT_DTO_LIST_TYPE = new TypeToken<List<ContactDTO>>() {
    }.getType();

    @Inject
    private ModelMapper mapper;

    @Inject
    private ContactService service;

    /**
     * Search for just one Contact by it's ID.
     *
     * @param id - of the Contact
     * @return Response
     */
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Operation(
            summary = "Get a contact by id",
            tags = "Contacts",
            description = "Returns just one Contact by it's ID for the currently logged in user.",
            responses = {
                    @ApiResponse(description = "The contact",
                            content = @Content(schema = @Schema(implementation = ContactDTO.class))
                    ),
                    @ApiResponse(responseCode = "404", description = "Contact not found")
            }
    )
    public Response retrieveContactById(
            @Parameter(
                    in = ParameterIn.PATH,
                    required = true,
                    description = "The unique ID of the contact",
                    schema = @Schema(type = "long")
            )
            @PathParam("id") long id) {
        Contact contact = service.findById(id);
        if (contact == null) {
            throw new WebApplicationException("Contact not found", Response.Status.NOT_FOUND);
        }

        log.info("findById " + id + ": found Contact = " + contact.getFirstName() + " " + contact.getLastName() + " " + contact.getEmail() + " " + contact.getPhoneNumber() + " "
                + " " + contact.getId());

        return Response.ok(mapper.map(contact, ContactDTO.class)).build();
    }


    /**
     * Returns the contacts for currently logged in user
     *
     * @return List of contacts
     */
    @GET
    @Operation(
            summary = "Get a list of contacts",
            tags = "Contacts",
            description = "Returns a list of contacts who belongs for the currently logged in user.",
            responses = @ApiResponse(
                    description = "The contacts",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = ContactDTO.class)))
            )
    )
    public Response retrieveContactsByUserAccount() {
        final List<Contact> allContactsByUserAccount = service.getContactsForCurrentUser();

        log.info("retrieveContactsByUserAccount: found " + allContactsByUserAccount.size() + " items");

        return Response.ok(mapper.map(allContactsByUserAccount, CONTACT_DTO_LIST_TYPE)).build();
    }

    /**
     * Save new contact for currently logged in user
     *
     * @param contactDTO - the contact to be saved
     * @return Response
     */
    @Operation(
            summary = "Save a new contact",
            tags = "Contacts",
            description = "Save a new contact for currently logged in user",
            responses = {
                    @ApiResponse(description = "Returns nothing")
            }
    )
    @POST
    public Response saveNewContact(
            @Valid ContactDTO contactDTO) {
        service.create(mapper.map(contactDTO, Contact.class));
        return Response.status(Response.Status.CREATED).build();
    }

    /**
     * Update contact which belongs for currenlty logged in user by its ID
     *
     * @param contactId  - the contact primary key Id
     * @param contactDTO - the contact to be updated
     * @return Response
     */
    @Operation(
            summary = "Update contact",
            tags = "Contacts",
            description = "Update a contact which belongs to currently logged in user",
            responses = {
                    @ApiResponse(description = "Returns nothing"),
                    @ApiResponse(responseCode = "503", description = "Could return status FORBIDDEN if try to update " +
                            "a Contact which doesn't belong to the logged in user")
            })
    @PUT
    @Path("{contactId}")
    public Response updateContact(
            @Parameter(
                    in = ParameterIn.PATH,
                    required = true,
                    description = "The unique ID of the Contact to be updated",
                    schema = @Schema(type = "long")
            )
            @PathParam("contactId") Long contactId, @Valid ContactDTO contactDTO) {
        service.update(contactId, mapper.map(contactDTO, Contact.class));
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    /**
     * Delete contact by its ID
     *
     * @param contactId - the contact's primary key ID
     * @return Response
     */
    @Operation(
            summary = "Delete contact",
            tags = "Contacts",
            description = "Delete a contact which belongs to currently logged in user",
            responses = {
                    @ApiResponse(description = "Returns nothing"),
                    @ApiResponse(responseCode = "503", description = "Could return status FORBIDDEN if try to delete " +
                            "a Contact which doesn't belong to the logged in user")
            })
    @DELETE
    @Path("{contactId}")
    public Response removeContact(
            @Parameter(
                    in = ParameterIn.PATH,
                    required = true,
                    description = "The unique ID of the Contact to be deleted",
                    schema = @Schema(type = "long")
            )
            @PathParam("contactId") Long contactId) {
        service.deleteById(contactId);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    /**
     * Filter the contacts by given search string.
     * The search string may take value for any of the following fields: First name, Last name, Phone or Mobile phone
     *
     * @param filterValue - the search string
     * @return The found list of Contacts
     */
    @Operation(
            summary = "Filter contacts",
            tags = "Contacts",
            description = "Filters contacts by FirstName, LastName, Phone or Mobilephone",
            responses = @ApiResponse(description = "The found contacts",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = ContactDTO.class)))
            )
    )
    @GET
    @Path("filter")
    public Response filterContacts(
            @Parameter(
                    in = ParameterIn.QUERY,
                    required = true,
                    description = "The string value by which the contacts will be filtered. It could contain the value of any of the following fields: firstName, lastName, phoneNumber, mobilePhoneNumber",
                    schema = @Schema(type = "string")
            )
            @QueryParam("filter") String filterValue) {
        final List<Contact> filtered = service.filter(filterValue);
        return Response.ok(mapper.map(filtered, CONTACT_DTO_LIST_TYPE)).build();
    }

    /**
     * Returns contacts in CSV format
     *
     * @return text plain in CSV format
     */
    @Operation(
            summary = "Returns the contacts for currently logged in user in CSV format",
            responses = @ApiResponse(
                    description = "The found contacts",
                    content = @Content(mediaType = MEDIA_TYPE_CSV)
            )
    )
    @GET
    @Path("export/csv")
    @Produces(MEDIA_TYPE_CSV)
    public Response exportContactsAsCSV() {
        return retrieveContactsByUserAccount();
    }
}
