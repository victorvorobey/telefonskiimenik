package si.src.testtask.imenikapi.security;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@RequestScoped
public class SecurityContext {

    @Inject
    private HttpServletRequest request;

    public String getCurrentAccountId() {
        Principal userPrincipal = request.getUserPrincipal();
        return userPrincipal != null ? userPrincipal.getName() : "";
    }
}
