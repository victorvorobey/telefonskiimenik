package si.src.testtask.imenikapi;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class JaxRsConfig extends Application {
}
