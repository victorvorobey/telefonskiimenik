package si.src.testtask.imenikapi.entity;

import java.io.Serializable;

public interface IPersistent<PK extends Serializable> extends Serializable {
    PK getId();

    void setId(PK id);
}
