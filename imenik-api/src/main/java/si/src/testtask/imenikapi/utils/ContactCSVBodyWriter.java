package si.src.testtask.imenikapi.utils;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import si.src.testtask.common.dto.ContactDTO;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;

import static si.src.testtask.imenikapi.utils.ContactCSVBodyWriter.MEDIA_TYPE_CSV;

/**
 * A custom Body writer to provide ability to return CSV format by JAX-RS
 */
@Provider
@Produces(MEDIA_TYPE_CSV)
public class ContactCSVBodyWriter implements MessageBodyWriter<List<ContactDTO>> {

    public static final String MEDIA_TYPE_CSV = "text/csv";

    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return List.class.isAssignableFrom(aClass);
    }

    @Override
    public long getSize(List<ContactDTO> data, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return 0;
    }

    @Override
    public void writeTo(List<ContactDTO> data, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap, OutputStream outputStream) throws IOException, WebApplicationException {
        if (data != null && !data.isEmpty()) {
            CsvMapper mapper = new CsvMapper();
            final ContactDTO contactDTO = data.get(0);
            final CsvSchema columns = mapper.schemaFor(contactDTO.getClass()).withHeader();
            mapper.writer(columns).writeValue(outputStream, data);
        }
    }
}
