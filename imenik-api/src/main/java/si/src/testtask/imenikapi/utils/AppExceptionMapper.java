package si.src.testtask.imenikapi.utils;

import lombok.extern.java.Log;
import org.hibernate.validator.internal.engine.path.PathImpl;
import si.src.testtask.imenikapi.exception.ApplicationException;

import javax.ejb.EJBAccessException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.HashMap;
import java.util.Map;

@Provider
@Log
public class AppExceptionMapper implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable e) {

        ConstraintViolationException ex = null;
        if (e instanceof ConstraintViolationException) {
            ex = (ConstraintViolationException) e;
        } else if (e.getCause() instanceof ConstraintViolationException) {
            ex = (ConstraintViolationException) e.getCause();
        }

        // Validation case
        if (ex != null) {
            Map<String, String> validationErrors = new HashMap<>();
            for (ConstraintViolation<?> constraintViolation : ex.getConstraintViolations()) {
                final String path = ((PathImpl) constraintViolation.getPropertyPath()).getLeafNode().asString();
                validationErrors.put(path, constraintViolation.getMessage());
            }

            return Response.status(Response.Status.BAD_REQUEST).entity(validationErrors).build();
        }

        // Security case
        if (e instanceof EJBAccessException) {
            Map<String, String> rv = new HashMap<>(1);
            rv.put("error", "you don't have access to this resource");
            return Response.status(Response.Status.FORBIDDEN).entity(rv).build();
        }

        throw new ApplicationException("Unexpected error in application", e);
    }
}
