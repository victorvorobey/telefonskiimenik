package si.src.testtask.imenikapi.dao;

import si.src.testtask.imenikapi.entity.Contact;
import si.src.testtask.imenikapi.utils.CryptoConverter;

import javax.persistence.TypedQuery;
import java.util.List;

public class ContactDAO extends GenericDAO<Long, Contact> {

    /**
     * Returns all contacts which belong to a particular account
     *
     * @param accountId - account unique id
     * @return  List of Contacts
     */
    public List<Contact> findAllContactsByUserAccount(String accountId) {
        final TypedQuery<Contact> namedQuery = em.createNamedQuery(Contact.FIND_BY_ACCOUNT, Contact.class);
        namedQuery.setParameter("accountId", accountId);
        return namedQuery.getResultList();
    }

    /**
     * Returns contacts filtered by the given filterValue which belong to a particular account
     *
     * @param accountId - account unique id
     * @param filterValue - filter request
     * @return List of Contacts
     */
    public List<Contact> filter(final String accountId, final String filterValue) {
        final TypedQuery<Contact> namedQuery = em.createNamedQuery(Contact.FILTER_BY_FIRST_LAST_OR_PHONE, Contact.class);
        namedQuery.setParameter(1, CryptoConverter.MY_SUPER_SECRET_KEY);
        namedQuery.setParameter(2, accountId);
        namedQuery.setParameter(3, "%" + filterValue + "%" );
        return namedQuery.getResultList();
    }
}
