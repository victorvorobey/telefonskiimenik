package si.src.testtask.imenikapi.dao;

import si.src.testtask.imenikapi.entity.IPersistent;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.logging.Logger;

public abstract class GenericDAO<PK extends Serializable, ENTITY extends IPersistent<PK>> {

    protected Logger log = Logger.getLogger(getClass().getName());

    @Inject
    protected EntityManager em;
    private Class<ENTITY> entityClass;


    public GenericDAO() {
        entityClass = (Class<ENTITY>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[1];
    }

    @PostConstruct
    public void init() {
        log.info("Created DAO for " + entityClass.getName() + " class");
    }

    /**
     * Find just one Entity by it's ID.
     *
     * @param id the primary key
     * @return - entity instance
     */
    public ENTITY findById(PK id) {
        return em.find(entityClass, id);
    }

    /**
     * Persists a new entity
     *
     * @param e the entity to be saved
     */
    public void persist(ENTITY e) {
        em.persist(e);
    }

    /**
     * Update an entity
     *
     * @param e the entity to be updated
     */
    public ENTITY update(ENTITY e) {
        return em.merge(e);
    }

    /**
     * Delete an entity
     *
     * @param e the entity to be deleted
     */
    public void delete(ENTITY e) {
        em.remove(e);
    }

    /**
     * Delete entity by its primary key ID
     *
     * @param id the primary key
     */
    public void deleteById(PK id) {
        final ENTITY byId = findById(id);
        delete(byId);
    }
}
