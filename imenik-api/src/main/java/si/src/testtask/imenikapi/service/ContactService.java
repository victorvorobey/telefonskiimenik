package si.src.testtask.imenikapi.service;

import org.apache.commons.lang3.StringUtils;
import si.src.testtask.imenikapi.dao.ContactDAO;
import si.src.testtask.imenikapi.entity.Contact;
import si.src.testtask.imenikapi.security.SecurityContext;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;

public class ContactService {

    @Inject
    private SecurityContext securityContext;

    @Inject
    private ContactDAO dao;

    /**
     * Returns contacts which belongs for the currently logged in user only.
     *
     * @return List of Contacts
     */
    public List<Contact> getContactsForCurrentUser() {
        final String currentAccountId = securityContext.getCurrentAccountId();
        if (StringUtils.isBlank(currentAccountId)) {
            return Collections.emptyList();
        }

        return dao.findAllContactsByUserAccount(currentAccountId);
    }

    /**
     * Find just one Contact by it's ID.
     *
     * @param id the unique id
     * @return Contact
     */
    public Contact findById(Long id) {
        final Contact contact = dao.findById(id);
        makeSureThatContactBelongsForTheCurrentUser(contact);
        return contact;
    }

    @Transactional
    public void create(Contact contact) {
        assignAccountId(contact);
        dao.persist(contact);
    }

    @Transactional
    public Contact update(Long id, Contact contact) {
        final Contact byId = dao.findById(id);
        makeSureThatContactBelongsForTheCurrentUser(byId);

        contact.setId(id);
        assignAccountId(contact);
        return dao.update(contact);
    }

    @Transactional
    public void deleteById(Long id) {
        final Contact byId = dao.findById(id);
        makeSureThatContactBelongsForTheCurrentUser(byId);
        dao.deleteById(id);
    }


    public List<Contact> filter(String filterValue) {
        final String currentAccountId = securityContext.getCurrentAccountId();
        return dao.filter(currentAccountId, filterValue);
    }

    private void makeSureThatContactBelongsForTheCurrentUser(final Contact contact) {
        if (contact != null) {
            if (!securityContext.getCurrentAccountId().equals(contact.getAccountId())) {
                throw new WebApplicationException(Response.Status.FORBIDDEN);
            }
        }
    }

    private void assignAccountId(Contact contact) {
        if (contact != null) {
            contact.setAccountId(securityContext.getCurrentAccountId());
        }
    }
}
