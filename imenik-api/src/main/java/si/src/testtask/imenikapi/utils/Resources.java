package si.src.testtask.imenikapi.utils;

import org.modelmapper.ModelMapper;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class Resources {

    @Produces
    @PersistenceContext(unitName = "imenikApiPu")
    private EntityManager em;

    @Produces
    public ModelMapper produceModelMapper() {
        return new ModelMapper();
    }
}
