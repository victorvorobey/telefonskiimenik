#!/usr/bin/env bash

/etc/init.d/mysql start && \
screen -dmS keycloak $KEYCLOAK_HOME/bin/standalone.sh -Djboss.socket.binding.port-offset=100 -b=0.0.0.0 && \
$WILDFLY_HOME/bin/standalone.sh -b=0.0.0.0